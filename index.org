#+TITLE: Code
#+AUTHOR: i4k
#+OPTIONS: html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="assets/styles.css" />

This document should be an accessible & consise introduction to the web
technologies, used in other things to make websites.

You can find the full code + content to make this page, at the following address:

- [[https://gitlab.com/sctlib/code][https://gitlab.com/sctlib/code]]

Feel free to come up with questions, suggestions, comments,
improvements (use the "issue" tab, on the page linked above).

* Table of Content
** Languages
- [[file:notes/html.org][HTML]]
- [[file:notes/css.org][CSS]]
- [[file:notes/javascript.org][Javascript]]

** Other
- [[file:notes/passwords.org][passwords]]
- [[file:notes/git-gitlab-github.org][git, gitlab, github]] etc.
- [[file:notes/ssh.org][ssh]]
